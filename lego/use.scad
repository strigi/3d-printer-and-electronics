use <lego-block.scad>;


translate([-15, 0]) brick(
    width = 1,
    height = 1,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([5, 0]) brick(
    width = 2,
    height = 1,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-20, 10]) brick(
    width = 3,
    height = 1,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([20, 10]) brick(
    width = 4,
    height = 1,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([0, 20]) brick(
    width = 6,
    height = 1,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-10, 40]) brick(
    width = 2,
    height = 2,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([20, 40]) brick(
    width = 3,
    height = 2,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([30, 60]) brick(
    width = 4,
    height = 2,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-20, 60]) brick(
    width = 6,
    height = 2,
    depth = 3
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([0, -10]) brick(
    width = 1,
    height = 1,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-15, -10]) brick(
    width = 2,
    height = 1,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([20, -20]) brick(
    width = 3,
    height = 1,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-20, -20]) brick(
    width = 4,
    height = 1,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([0, -30]) brick(
    width = 6,
    height = 1,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-20, -45]) brick(
    width = 2,
    height = 2,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([10, -45]) brick(
    width = 3,
    height = 2,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([-25, -65]) brick(
    width = 4,
    height = 2,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);

translate([25, -65]) brick(
    width = 6,
    height = 2,
    depth = 1
    //bumps = "H"
    //bulkheads = 2,
    //strips = true
);