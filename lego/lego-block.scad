/*
 * TODO: flat blocks do not need strips by default (they don't snap very well)
 */

$fn=64;
BOOLEAN_SLACK = 0.1;

BASE_DEPTH = 3.2; // Height of a "flat" lego brick (1/3 of a tall height)
BASE_WIDTH_HEIGHT = 8; // Width and height of a standard unit 1*1 brick
TOLERANCE_REDUCTION_WIDTH_HEIGHT = 0.1;
WALL_THICKNESS = 1.2;
INSIDE_STRIP_WIDTH = 0.6;
BUMP_HEIGHT = 1.8;

module brick(width = 4, height = 2, depth = 3, bumps = "S", center = true, bulkheads = true, strips = true) {
    bulkheadDistance = bulkheads == true && (width == 3 || height == 3) ? 3 : 2;    

    baseDimensions = calculateBaseDimensions(width, height, depth);
    hollowDimensions = calculateHollowDimensions(width, height, depth);
    
    centerTranslation = [-baseDimensions[0] / 2, -baseDimensions[1] / 2];
    translate(centerTranslation) {
        difference() {
            cube(baseDimensions);
            translate([WALL_THICKNESS, WALL_THICKNESS, -BOOLEAN_SLACK])
                cube(hollowDimensions + [0, 0, BOOLEAN_SLACK]);
        }
        
        if(strips) {
            strips(width, height, depth);
        }
        
        if(bulkheadDistance && (bulkheadDistance < width || bulkheadDistance < height)) {
            difference() {
                bulkheads(width, height, depth, frequency = bulkheadDistance);
                tubes(width, height, depth, hollow = false);
            }
        }
        tubes(width, height, depth, hollow = true);
        
            
        bumps(width, height, depth, concat([], bumps));
    }
}

module bump(hollow = false) {
    BUMP_DIAMETER = 4.8 + 0.1; // Corrected
    HOLLOW_BUMP_THICKNESS = 0.8;
    difference() {
        cylinder(
            d = BUMP_DIAMETER,
            h = BUMP_HEIGHT + BOOLEAN_SLACK
        );
        if(hollow) {
            translate([ 0, 0, BOOLEAN_SLACK]) cylinder(
                d = BUMP_DIAMETER - 2 * HOLLOW_BUMP_THICKNESS,
                h = BUMP_HEIGHT + 2 * BOOLEAN_SLACK
            );
        }
    }
}

module smallTube(height, hollow=true) {
    smallTubeDiameter = 3;
    smallTubeInnerDiameter = 1.3;
    difference() {
        cylinder(h = height, d = smallTubeDiameter);
        if(hollow) {
            translate([0, 0, -BOOLEAN_SLACK]) cylinder(
                h = height + 2 * BOOLEAN_SLACK,
                d = smallTubeInnerDiameter
            );
        }
    }
}

module largeTube(height, hollow=true) {
    insideTubeOuterDiameter = 6.51 - 0.1; // Corrected
    insideTubeInnerDiameter = 4.8 + 0.2; // Corrected    
    difference() {
        cylinder(h = height, d = insideTubeOuterDiameter);
        if(hollow) {
            translate([0, 0, -BOOLEAN_SLACK]) cylinder(
                h = height + 2 * BOOLEAN_SLACK,
                d = insideTubeInnerDiameter
            );
        }
    }
}

function calculateBaseDimensions(width, height, depth) = [
    (width * BASE_WIDTH_HEIGHT) - 2 * TOLERANCE_REDUCTION_WIDTH_HEIGHT,
    (height * BASE_WIDTH_HEIGHT) - 2 * TOLERANCE_REDUCTION_WIDTH_HEIGHT,
    depth * BASE_DEPTH
];

function calculateHollowDimensions(width, height, depth) = 
    calculateBaseDimensions(width, height, depth) - [
        2 * WALL_THICKNESS,
        2 * WALL_THICKNESS,
        WALL_THICKNESS
];

module bulkheads(width, height, depth, frequency) {
    hollowDimensions = calculateHollowDimensions(width, height, depth);
    
    bulkheadHeight = hollowDimensions[2] - BUMP_HEIGHT;
    
    for(x = [frequency : frequency : width - 1]) {
        translate([
            x * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT - INSIDE_STRIP_WIDTH / 2,
            WALL_THICKNESS,
            BUMP_HEIGHT
        ]) cube([
            INSIDE_STRIP_WIDTH,
            hollowDimensions[1],
            bulkheadHeight
        ]);
    }
    for(y = [frequency : frequency : height - 1]) {
        translate([
            WALL_THICKNESS,
            y * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT - INSIDE_STRIP_WIDTH / 2,
            BUMP_HEIGHT
        ]) cube([
            hollowDimensions[0],
            INSIDE_STRIP_WIDTH,
            bulkheadHeight
        ]);
    }
}

module tubes(width, height, depth, hollow) {
    hollowDimensions = calculateHollowDimensions(width, height, depth);
    
    if(width > 1 && height > 1) {
        for(y = [1 : height - 1]) {
            for(x = [1 : width - 1]) {
                translate([
                    x * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    y * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                ]) largeTube(hollowDimensions[2]);
            }
        }
        
        for(y = [1 : height - 1]) {
            for(x = [1 : width - 1]) {
                translate([
                    x * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    y * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                ]) largeTube(hollowDimensions[2], hollow);
            }
        }
    } else if(width != 1 || height != 1) {
        if(width == 1) {            
            for(y = [1 : height -1]) {
                translate([
                    BASE_WIDTH_HEIGHT / 2 - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    y * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    0
                ]) smallTube(hollowDimensions[2], hollow = hollow);
            }
        } else {
            for(x = [1 : width - 1]) {
                translate([
                    x * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    BASE_WIDTH_HEIGHT / 2 - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    0
                ]) smallTube(hollowDimensions[2], hollow = hollow);
            }
        }        
    }
}

module strips(width, height, depth) {
    INSIDE_STRIP_THICKNESS = 0.3;    
    
    hollowDimensions = calculateHollowDimensions(width, height, depth);
    stripDimensions = [
        INSIDE_STRIP_WIDTH,
        INSIDE_STRIP_THICKNESS,
        hollowDimensions[2]
    ];

    for(x = [0.5 : width - 0.5]) {
        translate([
            x * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT - INSIDE_STRIP_WIDTH / 2,
            WALL_THICKNESS
        ]) {            
            cube(stripDimensions);
            translate([
                0,
                hollowDimensions[1] - INSIDE_STRIP_THICKNESS
            ]) cube(stripDimensions);
        }
    }
    for(y = [0.5 : height - 0.5]) {
        rotate(90, [0, 0, 1])
            translate([
                y * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT - INSIDE_STRIP_WIDTH / 2,
                -(WALL_THICKNESS + INSIDE_STRIP_THICKNESS)
            ]) {
                cube(stripDimensions);
                translate([
                    0,
                    -hollowDimensions[0] + INSIDE_STRIP_THICKNESS
                ]) cube(stripDimensions);
            }
    }    
}

/**
 * Draws top side bumps on a brick with the given dimensions, using the given bumps pattern.
 */
module bumps(width, height, depth, bumpsPattern) {
    for(y = [0 : height - 1]) {
        for(x = [0 : width - 1]) {
            bumpType = bumpsPattern[(y * width + x) % len(bumpsPattern)];
            if(bumpType == "H" || bumpType == "S") {
                translate([
                    (x + 0.5) * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    (y + 0.5) * BASE_WIDTH_HEIGHT - TOLERANCE_REDUCTION_WIDTH_HEIGHT,
                    depth * BASE_DEPTH
                ]) bump(bumpType == "H");            
            }
        }
    }
}

brick(
    width = 8,
    height = 2,
    depth = 3,
    bumps = ["S", "H", "S", "N", "N", "S", "H", "S"],
    //bulkheads = 2,
    strips = true
);
