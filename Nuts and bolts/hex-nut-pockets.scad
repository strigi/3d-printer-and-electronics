TOLERANCE = 0.5;

translate([90, 0, 0]) nut(16.7 + TOLERANCE, 8);      // M10
translate([50, 0, 0]) nut(12.7 + TOLERANCE, 6.2);    // M8
translate([20, 0, 0]) nut(7.75 + TOLERANCE, 3.8);    // M5
translate([0, 0, 0]) nut(5.3 + TOLERANCE, 2.3);      // M3
   
/**
 * @param width The distance between two flat sides of the nut.
 * @param height The height of the nut.
 */
module nut(width, height) {
    SIDES=6;
    
    BOTTOM_PADDING = 2;
    SIDE_PADDING = 5;
    
    innerRadius = width / 2;
    outerRadius = sqrt(pow(innerRadius, 2) + pow(tan(360 / SIDES / 2) * innerRadius, 2));
    
    boxSize = outerRadius * 2 + SIDE_PADDING;
    boxHeight = height + BOTTOM_PADDING;
    
    difference() {
        translate([-boxSize / 2, -boxSize / 2])
        cube([boxSize, boxSize, boxHeight]);
        translate([0, 0, BOTTOM_PADDING])
            cylinder(r=outerRadius, h=height + 1, $fn=SIDES);
    }
     
}