import network

ssid = 'MyNetwork'
password = 'MyPassword'

print('Connecting to network ' + ssid + ' ' + password)
interface = network.WLAN(network.STA_IF)
interface.active(True)
interface.connect(ssid, password)
while not interface.isconnected():
	pass
print('network config:', interface.ifconfig())


from dht import DHT22
from machine import Pin
from time import sleep

sensor = DHT22(Pin(16))

while True:
	sensor.measure()
	print('TEMPERATURE: ' + str(sensor.temperature()) + '°C RELATIVE HUMIDITY: ' + str(sensor.humidity()) + '%')
	sleep(10)
